/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcancia;

import alcancia.controlador.AlcanciaControlador;
import alcancia.modelo.Moneda;
import javax.swing.JOptionPane;

/**
 *
 * @author Jesus Jimenez
 */
public class Alcancia {

    static int opcion = -1;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AlcanciaControlador alcanciaControlador = AlcanciaControlador.getAlcanciaControlador();

        menuPrincipal(alcanciaControlador);
    }

    public static void menuPrincipal(AlcanciaControlador ac) {
        try {
            String lectura = JOptionPane.showInputDialog(null, "Elige opción:"
                    + "\n1.- Agregar Moneda(s)"
                    + "\n2.- Cantidad Monedas en mi Alcancia."
                    + "\n3.- Cantidad Monedas por denominación."
                    + "\n4.- Cantidad Dinero por denominación."
                    + "\n0.- Salir\n");
            opcion = Integer.parseInt(lectura);

            switch (opcion) {
                case 1:
                    menuAgregarMonedas(ac);
                    break;
                case 2:
                    cantidadMonedas(ac);
                    break;
                case 3:
                    cantidadMonedasDenominacion(ac);
                    break;
                case 4:
                    cantidadDineroDenominacion(ac);
                    break;
                case 0:
                    JOptionPane.showMessageDialog(null, "Vuelva Pronto!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opción no encontrada");
                    break;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Uoop! Error!");
//            opcion = -1;
            menuPrincipal(ac);
        }
    }

    public static void menuAgregarMonedas(AlcanciaControlador ac) {
        try {
            opcion = menuDenominacion();

            switch (opcion) {
                case 1:
                    ac.getListaMoneda().add(new Moneda(50));
                    JOptionPane.showMessageDialog(null, "Moneda de 50$ agregada");
                    menuAgregarMonedas(ac);
                    break;
                case 2:
                    ac.getListaMoneda().add(new Moneda(100));
                    JOptionPane.showMessageDialog(null, "Moneda de 100$ agregada");
                    menuAgregarMonedas(ac);
                    break;
                case 3:
                    ac.getListaMoneda().add(new Moneda(200));
                    JOptionPane.showMessageDialog(null, "Moneda de 200$ agregada");
                    menuAgregarMonedas(ac);
                    break;
                case 4:
                    ac.getListaMoneda().add(new Moneda(500));
                    JOptionPane.showMessageDialog(null, "Moneda de 500$ agregada");
                    menuAgregarMonedas(ac);
                    break;
                case 5:
                    ac.getListaMoneda().add(new Moneda(1000));
                    JOptionPane.showMessageDialog(null, "Moneda de 1000$ agregada");
                    menuAgregarMonedas(ac);
                    break;
                case 6:
                    menuPrincipal(ac);
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null, "Vuelva Pronto!");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opción no encontrada");
                    menuAgregarMonedas(ac);
                    break;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Uoop! Error!");
        }
    }

    public static void cantidadMonedasDenominacion(AlcanciaControlador ac) {
        opcion = menuDenominacion();
        switch (opcion) {
            case 1:
                JOptionPane.showMessageDialog(null, ac.cantidadMonedasDenominacion(50));
                cantidadMonedasDenominacion(ac);
                break;
            case 2:
                JOptionPane.showMessageDialog(null, ac.cantidadMonedasDenominacion(100));
                cantidadMonedasDenominacion(ac);
                break;
            case 3:
                JOptionPane.showMessageDialog(null, ac.cantidadMonedasDenominacion(200));
                cantidadMonedasDenominacion(ac);
                break;
            case 4:
                JOptionPane.showMessageDialog(null, ac.cantidadMonedasDenominacion(500));
                cantidadMonedasDenominacion(ac);
                break;
            case 5:
                JOptionPane.showMessageDialog(null, ac.cantidadMonedasDenominacion(1000));
                cantidadMonedasDenominacion(ac);
                break;
            case 6:
                menuPrincipal(ac);
                break;
            case 7:
                JOptionPane.showMessageDialog(null, "Vuelva Pronto!");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Opción no encontrada");
                cantidadMonedasDenominacion(ac);
                break;
        }
    }

    public static void cantidadDineroDenominacion(AlcanciaControlador ac) {
        opcion = menuDenominacion();
        switch (opcion) {
            case 1:
                JOptionPane.showMessageDialog(null, ac.cantidadDineroDenominacion(50));
                cantidadDineroDenominacion(ac);
                break;
            case 2:
                JOptionPane.showMessageDialog(null, ac.cantidadDineroDenominacion(100));
                cantidadDineroDenominacion(ac);
                break;
            case 3:
                JOptionPane.showMessageDialog(null, ac.cantidadDineroDenominacion(200));
                cantidadDineroDenominacion(ac);
                break;
            case 4:
                JOptionPane.showMessageDialog(null, ac.cantidadDineroDenominacion(500));
                cantidadDineroDenominacion(ac);
                break;
            case 5:
                JOptionPane.showMessageDialog(null, ac.cantidadDineroDenominacion(1000));
                cantidadDineroDenominacion(ac);
                break;
            case 6:
                menuPrincipal(ac);
                break;
            case 7:
                JOptionPane.showMessageDialog(null, "Vuelva Pronto!");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Opción no encontrada");
                cantidadDineroDenominacion(ac);
                break;
        }
    }

    public static void cantidadMonedas(AlcanciaControlador ac) {
        JOptionPane.showMessageDialog(null, "Hay " + ac.cantidadMonedas() + " Monedas actualmente!");
        menuPrincipal(ac);
    }

    public static int menuDenominacion() {
        try {
            String lectura = JOptionPane.showInputDialog(null, "\nElige Denominación:"
                    + "\n1.- 50$"
                    + "\n2.- 100$"
                    + "\n3.- 200$"
                    + "\n4.- 500$"
                    + "\n5.- 1000$"
                    + "\n6.- Atras"
                    + "\n7.- Salir");
            return Integer.parseInt(lectura);

        } catch (Exception e) {
            return 0;
        }
    }
}
