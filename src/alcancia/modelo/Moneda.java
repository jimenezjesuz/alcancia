/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcancia.modelo;

/**
 *
 * @author Jesus Jimenez
 */
public class Moneda {

    private int valor;

    public Moneda() {
    }

    public Moneda(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

}
