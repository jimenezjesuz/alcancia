/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alcancia.controlador;

import alcancia.modelo.Moneda;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jesus Jimenez
 */
public class AlcanciaControlador {

    private static AlcanciaControlador alcanciaControlador;
    private List<Moneda> listaMoneda;

    public static AlcanciaControlador getAlcanciaControlador() {
        if (alcanciaControlador == null) {
            alcanciaControlador = new AlcanciaControlador();
        }
        return alcanciaControlador;
    }

    private AlcanciaControlador() {
        this.listaMoneda = new ArrayList<>();
    }

    public List<Moneda> getListaMoneda() {
        return listaMoneda;
    }

    public int cantidadMonedas() {
        return this.listaMoneda.size();
    }

    public String cantidadMonedasDenominacion(int valor) {
        int cont = 0;
        for (Moneda m : listaMoneda) {
            if (m.getValor() == valor) {
                cont++;
            }
        }
        return "Existen actualmente " + cont + " monedas de la denominación " + valor;
    }

    public String cantidadDineroDenominacion(int valor) {
        int total = 0;
        for (Moneda m : listaMoneda) {
            if (m.getValor() == valor) {
                total = total + valor;
            }
        }
        return "Existen actualmente " + total + " de dinero por la denominación " + valor;

    }

}
